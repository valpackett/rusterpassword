[![crates.io](https://img.shields.io/crates/v/rusterpassword.svg)](https://crates.io/crates/rusterpassword)
[![API Docs](https://docs.rs/rusterpassword/badge.svg)](https://docs.rs/rusterpassword/)
[![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

# rusterpassword

A [Rust] implementation of the [Spectre / Master Password algorithm].

Uses [secstr] secure strings and the [RustCrypto] ecosystem crates.

Also includes a C API for calling from other languages.

[Rust]: https://www.rust-lang.org
[Spectre / Master Password algorithm]: https://spectre.app/spectre-algorithm.pdf
[secstr]: https://crates.io/crates/secstr
[RustCrypto]: https://github.com/RustCrypto

## Usage

```rust
use secstr::*;
use rusterpassword::*;

fn main() {
    let master_key = gen_master_key(SecStr::from("Correct Horse Battery Staple"), "Cosima Niehaus").unwrap();
    let site_seed = gen_site_seed(&master_key, "twitter.com", 5).unwrap();
    let password = gen_site_password(site_seed, TEMPLATES_MAXIMUM);
}
```

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).
